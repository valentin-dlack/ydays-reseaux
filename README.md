# Exercice - Adressage IP Ydays

## *Valentin DAUTREMENT*

**Sommaire :**

- [Exercices](#exercices)
    - [IP n°1](#ip-1-1921680024)
    - [IP n°2](#ip-2-100008)
    - [IP n°3](#ip-3-172160025525500)
    - [IP n°4](#ip-4-192168255030)
    - [IP n°5](#ip-5-10500022)
    - [IP n°6](#ip-6-172280025524800)

## Exercices

### IP 1 : `192.168.0.0/24`

- Conversion de l'adresse IP décimale -> Binaire  

    1er octet : 192 = 128 + 64 = 1100 0000  
    2eme octet : 168 = 128 + 32 + 8 = 1010 1000  

    Résultat :  `1100 0000.1010 1000.0000 0000.0000 0000`  

- Calcul du masque de sous-réseau :

    Notation CIDR -> `/24` donc il y a 24 bits à 1  
    Résultat binaire : `1111 1111.1111 1111.1111 1111.0000 0000`  
    Résultat décimal : `255.255.255.0`  

- Calcul de l'adresse réseau :

    On utilise le ET Logique avec l'IP et le masque pour obtenir cette adresse :  

  IP (binaire) ---> : `1100 0000.1010 1000.0000 0000.0000 0000`  
  Masque (binaire) : `1111 1111.1111 1111.1111 1111.0000 0000`  
  _  
  Résultat ET logique : **11**00 0000.**1**0**1**0 **1**000.0000 0000.0000 0000  
  
  Résultat décimal : 192.168.0.0  
  L'adresse que l'on nous donne est donc l'adresse réseau.  
  
- Calcul de l'adresse broadcast :

    Cette fois j'utilise le OU Logique :  

    IP (Binaire) ---> :  `1100 0000.0100 0100.0000 0000.0000 0000`  
    Broadcast (Binaire) : `1100 0000.0100 0100.0000 0000.1111 1111`  
    _  
    Résultat OU Logique : 1100 0000.0100 0100.0000 0000.**1111 1111**  

    L'adresse broadcast est donc : `192.168.0.255`  

- Nombre d'adresses client possible :

    Il y a 256 adresses client possible.  

    Pour trouver les adresses disponibles ça il suffit de faire 2^(bits disponibles).  
    Soit pour nous : `2^8 = 256`  

- Nombre d'adresses client possible sur le réseau :

    Pour les adresses possible sur le réseau on soustrait 2 car on exclu l'adresse réseau et broadcast.  
    Sachant que notre masque est en /24, il y'a 254 adresses possible sur le réseau.  

- Plage d'adresse réseau :

    La plage d'adresse du réseau est de `192.168.0.1 <-à-> 192.168.0.254`

### IP 2 : `10.0.0.0/8`

- Conversion de l'adresse IP décimale -> Binaire

    1er octet : 10 = 8 + 2 = 0000 1010  

    Résultat :  `0000 1010.0000 0000.0000 0000.0000 0000`

- Calcul du masque de sous-réseau :

    Notation CIDR -> `/8` donc il y a 8 bits à 1  
    Résultat binaire : `1111 1111.0000 0000.0000 0000.0000 0000`  
    Résultat décimal : `255.0.0.0`

- Calcul de l'adresse réseau :

    On utilise le ET Logique avec l'IP et le masque pour obtenir cette adresse :

  IP (binaire) ---->  : `0000 1010.0000 0000.0000 0000.0000 0000`  
  Masque (binaire) : `1111 1111.0000 0000.0000 0000.0000 0000`  
  _  
  Résultat ET logique : 0000 **1**0**1**0.0000 0000.0000 0000.0000 0000
  
  Résultat décimal : 10.0.0.0  
  L'adresse que l'on nous donne est donc l'adresse réseau.
  
- Calcul de l'adresse broadcast :

    Cette fois j'utilise le OU Logique :

    IP (Binaire) ---> : `0000 1010.0000 0000.0000 0000.0000 0000`  
    Broadcast (Binaire) : `0000 1010.1111 1111.1111 1111.1111 1111`  
    _  
    Résultat OU Logique : 0000 1010.**1111 1111**.**1111 1111**.**1111 1111**

    L'adresse broadcast est donc : `10.255.255.255`

- Nombre d'adresses client possible :

    Il y a `16 777 216` adresses client disponible.  

    Pour trouver les adresses disponibles ça il suffit de faire 2^(bits disponibles).  
    Soit pour nous : `2^24 = 16 777 216`  

- Nombre d'adresses client possible sur le réseau :

    Pour les adresses possible sur le réseau on soustrait 2 car on exclu l'adresses réseau et broadcast.  
    Sachant que notre masque est en /8, il y'a `16 777 214` adresses possible sur le réseau.

- Plage d'adresse réseau :

    La plage d'adresse du réseau est de `10.0.0.1 <-à-> 10.255.255.254`

### IP 3 : `172.16.0.0/255.255.0.0`

- Conversion de l'adresse IP décimale -> Binaire

    1er octet : 172 = 128 + 32 + 8 + 4 = 1010 1100  
    2ème octet : 16 = 0001 0000

    Résultat :  `1010 1100.0001 0000.0000 0000.0000 0000`

- Calcul du masque de sous-réseau :

    Dans cet exercice, le masque est déjà sous forme décimal :
    `255.255.0.0`  
    Résultat binaire : `1111 1111.1111 1111.0000 0000.0000 0000`  
    Résultat CIDR : `/16`, il y a 16 bits à 1

- Calcul de l'adresse réseau :

    On utilise le ET Logique avec l'IP et le masque pour obtenir cette adresse :

  IP (binaire) ---> : `1010 1100.0001 0000.0000 0000.0000 0000`  
  Masque (binaire) : `1111 1111.1111 1111.0000 0000.0000 0000`  
  _  
  Résultat ET logique : **1**0**1**0 **11**00.000**1** 0000.0000 0000.0000 0000
  
  Résultat décimal : 172.16.0.0
  L'adresse que l'on nous donne est donc l'adresse réseau.
  
- Calcul de l'adresse broadcast :

    Cette fois j'utilise le OU Logique :

    IP (Binaire) ---> : `1010 1100.0001 0000.0000 0000.0000 0000`  
    Broadcast (Binaire) : `1010 1100.0001 0000.1111 1111.1111 1111`  
    _  
    Résultat OU Logique : 0000 1010.0001 0000.**1111 1111**.**1111 1111**

    L'adresse broadcast est donc : `172.16.255.255`

- Nombre d'adresses client possible :

    Il y a `65 536` adresses client possible

    Pour trouver les adresses possible ça il suffit de faire 2^(bits disponibles).  
    Soit pour nous : `2^16 = 65 536`

- Nombre d'adresses client possible sur le réseau :

    Pour les adresses utilisable on soustrait 2 car on exclu l'adresses réseau et broadcast.  
    Sachant que notre masque est en /8, il y'a `65 534` adresses possible sur le réseau.

- Plage d'adresse réseau :

    La plage d'adresse du réseau est de `172.16.0.1 <-à-> 172.16.255.254`

### IP 4 : `192.168.255.0/30`

- Conversion de l'adresse IP décimale -> Binaire

    1er octet : 192 = 128 + 64 = 1100 0000  
    2ème octet : 168 = 128 + 32 + 8 = 1010 1000  
    3ème octet : 255 = 1111 1111

    Résultat :  `1100 0000.1010 1000.1111 1111.0000 0000`  

- Calcul du masque de sous-réseau :

    Notation CIDR -> `/30` donc il y a 30 bits à 1  
    Résultat binaire : `1111 1111.1111 1111.1111 1111.1111 1100`  
    Résultat décimal : `255.255.255.252`  

- Calcul de l'adresse réseau :

    On utilise le ET Logique avec l'IP et le masque pour obtenir cette adresse :

  IP (binaire) ---> : `1100 0000.1010 1000.1111 1111.0000 0000`  
  Masque (binaire) : `1111 1111.1111 1111.1111 1111.1111 1100`  
  _  
  Résultat ET logique : **11**00 000.**1**0**1**0 **1**000.**1111 1111**.0000 0000
  
  Résultat décimal : 192.168.255.0  
  L'adresse que l'on nous donne est donc l'adresse réseau.
  
- Calcul de l'adresse broadcast :

    Cette fois j'utilise le OU Logique :

    IP (Binaire) ---> : `1100 0000.1010 1000.1111 1111.0000 0000`  
    Broadcast (Binaire) : `1100 0000.1010 1000.0000 0000.000011`  
    _  
    Résultat OU Logique : 1100 0000.1010 1000.1111 1111.0000 00**11**

    L'adresse broadcast est donc : `192.168.255.3`

- Nombre d'adresses client possible :

    Il y a `4` adresses client possible

    Pour trouver les adresses possible ça il suffit de faire 2^(bits disponibles).  
    Soit pour nous : `2^2 = 4`

- Nombre d'adresses client possible sur le réseau :

    Pour les adresses utilisable on soustrait 2 car on exclu l'adresses réseau et broadcast.  
    Sachant que notre masque est en /30, il y'a `2` adresses possible sur le réseau.

- Plage d'adresse réseau :

    La plage d'adresse du réseau est de `192.168.255.1 <-à-> 192.168.255.2`

### IP 5 : `10.50.0.0/22`

- Conversion de l'adresse IP décimale -> Binaire

    1er octet : 10 = 8 + 2 = 0000 1010  
    2eme octet : 50 = 32 + 16 + 2 = 0011 0010

    Résultat :  `0000 1010.0011 0010.0000 0000.0000 0000`

- Calcul du masque de sous-réseau :

    Notation CIDR -> `/22` donc il y a 22 bits à 1  
    Résultat binaire : `1111 1111.1111 1111.1111 1100.0000 0000`  
    Résultat décimal : `255.255.252.0`

- Calcul de l'adresse réseau :

    On utilise le ET Logique avec l'IP et le masque pour obtenir cette adresse :

  IP (binaire) ---->  : `0000 1010.0011 0010.0000 0000.0000 0000`  
  Masque (binaire) : `1111 1111.1111 1111.1111 1100.0000 0000`  
  _  
  Résultat ET logique : 0000 **1**0**1**0.00**11** 00**1**0.0000 0000.0000 0000
  
  Résultat décimal : 10.50.0.0  
  L'adresse que l'on nous donne est donc l'adresse réseau.
  
- Calcul de l'adresse broadcast :

    Cette fois j'utilise le OU Logique :

    IP (Binaire) ---> : `0000 1010.0011 0010.0000 0000.0000 0000`  
    Broadcast (Binaire) : `0000 1010.0011 0010.0000 0011.1111 1111`  
    _  
    Résultat OU Logique : 0000 1010.0011 0010.0000 00**11**.**1111 1111**

    L'adresse broadcast est donc : `10.50.3.255`

- Nombre d'adresses client possible :

    Il y a `1024` adresses client disponibles.

    Pour trouver les adresses disponibles ça il suffit de faire 2^(bits disponibles).  
    Soit pour nous : `2^10 = 1024`  

- Nombre d'adresses client possible sur le réseau :

    Pour les adresses possible sur le réseau on soustrait 2 car on exclu l'adresses réseau et broadcast.  
    Sachant que notre masque est en /8, il y'a `1022` adresses possible sur le réseau.

- Plage d'adresse réseau :

    La plage d'adresse du réseau est de `10.50.0.1 <-à-> 10.50.3.254`.

### IP 6 : `172.28.0.0/255.248.0.0`

- Conversion de l'adresse IP décimale -> Binaire

    1er octet : 172 = 128 + 32 + 8 + 4 = 1010 1100  
    2ème octet : 28 = 16 + 8 + 4 = 0001 1100

    Résultat :  `1010 1100.0001 1100.0000 0000.0000 0000`

- Calcul du masque de sous-réseau :

    Dans cet exercice, le masque est déjà sous forme décimal :
    `255.248.0.0`  
    Résultat binaire : `1111 1111.1111 1000.0000 0000.0000 0000`  
    Résultat CIDR : `/13`, il y a 13 bits à 1

- Calcul de l'adresse réseau :

    On utilise le ET Logique avec l'IP et le masque pour obtenir cette adresse :

  IP (binaire) ---> : `1010 1100.0001 1100.0000 0000.0000 0000`  
  Masque (binaire) : `1111 1111.1111 1000.0000 0000.0000 0000`  
  _  
  Résultat ET logique : **1**0**1**0 **11**00.000**1** **1**000.0000 0000.0000 0000
  
  Résultat décimal : 172.24.0.0  
  L'adresse réseau est donc `172.24.0.0`
  
- Calcul de l'adresse broadcast :

    Cette fois j'utilise le OU Logique :

    IP (Binaire) ---> : `1010 1100.0001 1100.0000 0000.0000 0000`  
    Broadcast (Binaire) : `1010 1100.0001 1111.1111 1111.1111 1111`  
    _  
    Résultat OU Logique : 0000 1010.0001 **1111**.**1111 1111**.**1111 1111**

    L'adresse broadcast est donc : `172.31.255.255`

- Nombre d'adresses client possible :

    Il y a `524 288` adresses client possible

    Pour trouver les adresses possible ça il suffit de faire 2^(bits disponibles).  
    Soit pour nous : `2^19 = 524 288`

- Nombre d'adresses client possible sur le réseau :

    Pour les adresses utilisable on soustrait 2 car on exclu l'adresses réseau et broadcast.  
    Sachant que notre masque est en /8, il y'a `524 286` adresses possible sur le réseau.

- Plage d'adresse réseau :

    La plage d'adresse du réseau est de `172.24.0.1 <-à-> 172.31.255.254`
